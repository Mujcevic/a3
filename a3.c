#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "framework.h"

typedef enum _Direction_
{
  TOP,
  LEFT,
  BOTTOM,
  RIGHT
} Direction;
/*
typedef struct _Player_
{
  char* name;
  int points;
} Player;
*/
typedef struct _Highscore_
{
  char name[4];
  uint8_t score;
} Highscore;

uint8_t shiftLeft(uint8_t field);
uint8_t shiftRight(uint8_t field);
bool isDirectionOutOfMap(uint8_t width, uint8_t height, uint8_t coord[2], Direction dir);
bool isPipeOpenInDirection(uint8_t** map, uint8_t width, uint8_t height, uint8_t coord[2], Direction dir);
bool shouldPipeConnectInDirection(uint8_t** map, uint8_t width, uint8_t height, uint8_t coord[2], Direction dir);
uint8_t* getAdjacentPipe(uint8_t** map, uint8_t width, uint8_t height, uint8_t coord[2], Direction dir);
bool compareStringBegins(const char *first, const char *second);

int main (int argc, char **argv)
{

  int round = 1;
  char magic_number[8];

  if (argc != 2)
  {
    printf(USAGE_APPLICATION);
    return 1;
  }

  FILE* file = fopen(argv[1], "rb");

  if (file == NULL)
  {
    printf(ERROR_OPEN_FILE, argv[1]);
    return 2;
  }

  fread(magic_number, 1, 7, file);

  magic_number[7] = '\0';

  if (strcmp(magic_number, "ESPipes") != 0)
  {
    printf(ERROR_INVALID_FILE, argv[1]);
    return 3;
  }
    
  uint8_t width; 
  
  fread(&width, sizeof(uint8_t), 1, file);

  uint8_t height; 
  
  fread(&height, sizeof(uint8_t), 1, file);

  uint8_t start_pipe[2]; 
  
  fread(start_pipe, sizeof(uint8_t), 2, file);

  uint8_t end_pipe[2]; 
  
  fread(end_pipe, sizeof(uint8_t), 2, file);

  uint8_t number_of_entries; 
  
  fread(&number_of_entries, 1, 1, file);

  Highscore* highscore_list = (Highscore*) malloc (number_of_entries * sizeof(Highscore));

  for (uint8_t count = 0; count < number_of_entries; count++)
  {
    fread(&highscore_list[count].score, 1, 1, file);
    fread(&highscore_list[count].name, 1, 3, file);

    highscore_list[count].name[3] = '\0';
  }

  //uint8_t** map = (uint8_t**) malloc (sizeof(uint8_t) * height * width);
  
  uint8_t** map = (uint8_t **)malloc(height * sizeof(uint8_t *)); 
  
  for (uint8_t count = 0; count < height; count++)
    map[count] = (uint8_t *)malloc(width * sizeof(uint8_t)); 
    
  for (uint8_t count1 = 0; count1 < height; count1++)
  {
    for (uint8_t count2 = 0; count2 < width; count2++)
    {
      fread(&map[count1][count2], sizeof(uint8_t), 1, file);
    }
  }

  printMap(map, width, height, start_pipe, end_pipe);
  
  
  Command cmd;
  size_t dir;
  uint8_t row, col;
  
  char* value;
  int reset = 0;
  
  while (1 && !reset) 
  {
    printf(INPUT_PROMPT, round);

    char* user_input = getLine();
  
    
    value = parseCommand(user_input, &cmd, &dir, &row, &col);
    
    
    if (value == NULL)
    {
      if (cmd == 0) //NONE
      {
	printf("\n");
	
      }
      
      else if (cmd == 1) //ROTATE
      {
	if (row < 0 || row > height || col < 0 || col > width)
	{
	  printf(USAGE_COMMAND_ROTATE);
	  
        }
	
	else if ((row - 1 == start_pipe[0] && col - 1 == start_pipe[1]) || (row -1 == end_pipe[0] && col - 1 == end_pipe[1]))
	{
	  printf(ERROR_ROTATE_INVALID);
	  
	}
	
	else if (dir == 1)
	{
	  map[row-1][col-1] = shiftLeft(map[row-1][col-1]);
	  printMap(map, width, height, start_pipe, end_pipe);
	  
	  if (arePipesConnected(map, width, height, start_pipe, end_pipe))
	  {
	    printf(INFO_PUZZLE_SOLVED);
	    printf(INFO_SCORE, round);
	  }
	    
	  round++;
	  
	}

	else if (dir == 3)
	{
	  map[row-1][col-1] = shiftRight(map[row-1][col-1]);
	  printMap(map, width, height, start_pipe, end_pipe);
	  
	  if (arePipesConnected(map, width, height, start_pipe, end_pipe))
	  {
	    printf(INFO_PUZZLE_SOLVED);
	    printf(INFO_SCORE, round);
	  }
	   
	  round++;

	}
	
      }
      
      
      else if (cmd == 2) //HELP
      {
	printf(HELP_TEXT); 
      }
      
      else if (cmd == 3) //QUIT
      {
	free(value);
	fclose(file);
	
	for (int index = 0; index < height; index++)
	  free(map[index]);
	
	free(map);
	
	return 0;
      }
      
      else if (cmd == 4) //RESTART
      {
	
      }
    }
    
    else if (value == (char*) 1)
    {
      printf(USAGE_COMMAND_ROTATE);
      
    }
    
    else 
    {
      printf(ERROR_UNKNOWN_COMMAND, value);
    }
    
  
  }



  return 0;
}

uint8_t shiftLeft(uint8_t field)
{
  uint8_t temp = field;
  field = field >> 2;
  temp = temp << ((1 * CHAR_BIT) - 2);
  
  return field | temp;
  
}

uint8_t shiftRight(uint8_t field)
{
  uint8_t temp = field;
  field = field << 2;
  temp = temp >> ((1 * CHAR_BIT) - 2);
  
  return field | temp;
  
}


bool isDirectionOutOfMap(uint8_t width, uint8_t height, uint8_t coord[2], Direction dir)
{
  switch (dir)
  {
    case 0:
    if (coord[0] == 0)
       return true;
    else 
       return false;
    break;
    
    case 1:
    if (coord[1] == 0)
       return true;
    else 
       return false;
    break;
    
    case 2:
    if (coord[0] == height)
       return true; 
    else 
       return false;          
    break;
    
    case 3:
    if (coord[1] == width)
       return true;
    else 
       return false;
    break;
    
    default:
    return false;
  }
}

bool isPipeOpenInDirection(uint8_t** map, uint8_t width, uint8_t height, uint8_t coord[2], Direction dir)
{

}

bool shouldPipeConnectInDirection(uint8_t** map, uint8_t width, uint8_t height, uint8_t coord[2], Direction dir)
{

}

uint8_t* getAdjacentPipe(uint8_t** map, uint8_t width, uint8_t height, uint8_t coord[2], Direction dir)
{

}
